//
//  main.m
//  DD
//
//  Created by 周琪樺 on 17/9/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
