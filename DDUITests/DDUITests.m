//
//  DDUITests.m
//  DDUITests
//
//  Created by 周琪樺 on 17/9/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DDUITests-Swift.h"
@interface DDUITests : XCTestCase

@end

@implementation DDUITests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.

    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;

    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [Snapshot setupSnapshot:app waitForAnimations:YES];
    [app launch];

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
}

- (void)testExample1 {
    // Use recording to get started writing UI tests.
    
    
    [[[[[[XCUIApplication alloc] init] childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element tap];
    [Snapshot snapshot:@"01LoginScreen" timeWaitingForIdle:2];
    
}

- (void)testExample2{
    
    XCUIElement *element = [[[[[XCUIApplication alloc] init] childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element;
    [element tap];
    [[element childrenMatchingType:XCUIElementTypeOther].element tap];
    
    [Snapshot snapshot:@"02LoginScreen" timeWaitingForIdle:2];
    
}

- (void)testExample3{
    
    
    
    XCUIElement *window = [[[[XCUIApplication alloc] init] childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0];
    XCUIElement *element = [window childrenMatchingType:XCUIElementTypeOther].element;
    [element tap];
    [[element childrenMatchingType:XCUIElementTypeOther].element tap];
    [[[[window childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1] childrenMatchingType:XCUIElementTypeOther].element tap];
     [Snapshot snapshot:@"03LoginScreen" timeWaitingForIdle:2];
    
}
@end
